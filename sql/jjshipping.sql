/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 80021
Source Host           : 127.0.0.1:3306
Source Database       : jjshipping

Target Server Type    : MYSQL
Target Server Version : 80021
File Encoding         : 65001

Date: 2021-06-09 10:42:56
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `name` varchar(20) NOT NULL COMMENT '账号',
  `realname` varchar(10) NOT NULL COMMENT '名字',
  `password` varchar(20) NOT NULL COMMENT '密码',
  `is_delete` int NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------

-- ----------------------------
-- Table structure for box
-- ----------------------------
DROP TABLE IF EXISTS `box`;
CREATE TABLE `box` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '箱id',
  `box_id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '箱号',
  `order_id` int NOT NULL COMMENT '订单id',
  `lead_id` int NOT NULL COMMENT '铅封号',
  `box_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '箱型',
  `box_size` double NOT NULL COMMENT '箱子尺寸',
  `box_weight` double NOT NULL COMMENT '箱重',
  `is_delete` int NOT NULL COMMENT '是否删除',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of box
-- ----------------------------

-- ----------------------------
-- Table structure for box_fix_money
-- ----------------------------
DROP TABLE IF EXISTS `box_fix_money`;
CREATE TABLE `box_fix_money` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '修箱费号',
  `box_id` int NOT NULL COMMENT '箱号',
  `return_time` datetime NOT NULL COMMENT '还箱时间',
  `work_money` double NOT NULL COMMENT '工时费用',
  `material_monry` double NOT NULL COMMENT '材料费用',
  `ticket_id` varchar(20) NOT NULL COMMENT '发票号码',
  `receive_money` double NOT NULL COMMENT '收款',
  `receive_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '收款方式',
  `receive_person` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '收款人',
  `receive_time` datetime NOT NULL COMMENT '收款时间',
  `last_victivity` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '最后动态',
  `is_delete` int DEFAULT NULL COMMENT '是否删除',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of box_fix_money
-- ----------------------------

-- ----------------------------
-- Table structure for box_stag_money
-- ----------------------------
DROP TABLE IF EXISTS `box_stag_money`;
CREATE TABLE `box_stag_money` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '滞箱费号',
  `box_id` int NOT NULL COMMENT '箱id',
  `days_use` int NOT NULL COMMENT '使用天数',
  `days_free` int NOT NULL COMMENT '免费天数',
  `reduce_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '减免类型',
  `reduce_num` double NOT NULL COMMENT '减免数额',
  `currency` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '币种',
  `money_should` double NOT NULL COMMENT '应收费用',
  `money_reduce` double NOT NULL COMMENT '减免费用',
  `money_payed` double NOT NULL COMMENT '结算金额',
  `ticket_id` varchar(30) NOT NULL COMMENT '发票号码',
  `money_recieve` double NOT NULL COMMENT '收款',
  `recieve_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '收款方式',
  `recieve_person` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '收款人',
  `recieve_time` datetime NOT NULL COMMENT '收款时间',
  `is_delete` int NOT NULL COMMENT '是否删除',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of box_stag_money
-- ----------------------------

-- ----------------------------
-- Table structure for charges_demurrage
-- ----------------------------
DROP TABLE IF EXISTS `charges_demurrage`;
CREATE TABLE `charges_demurrage` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '滞期费id',
  `order_id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '提单号',
  `is_import` int NOT NULL DEFAULT '0' COMMENT '进出口，默认0,0为进口，1为出口',
  `operator` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '营运人',
  `charge_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '收费类目',
  `money_type` varchar(10) NOT NULL DEFAULT '1' COMMENT '币种',
  `money` double NOT NULL COMMENT '结算费用',
  `money_reduction` double NOT NULL COMMENT '费用减免',
  `money_receivable` double NOT NULL COMMENT '应收费用',
  `user_box` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用箱人',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '逻辑删除，0为未删除，1为已删除',
  `create_time` datetime NOT NULL COMMENT '记录创建时间',
  `update_time` datetime NOT NULL COMMENT '记录更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of charges_demurrage
-- ----------------------------

-- ----------------------------
-- Table structure for company_info
-- ----------------------------
DROP TABLE IF EXISTS `company_info`;
CREATE TABLE `company_info` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '公司信息id',
  `user_id` int NOT NULL COMMENT '用户id',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '公司名',
  `address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '地址',
  `fax` varchar(11) NOT NULL COMMENT '传真',
  `phone_num` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '电话',
  `business_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '营业执照编号',
  `ratepayer` int NOT NULL COMMENT '是否增值税一般纳税人',
  `tax_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '税务登记证编号',
  `card_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '主用户身份证号',
  `calling_card` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名片',
  `trade_type` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '贸易类型',
  `is_delete` int NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of company_info
-- ----------------------------

-- ----------------------------
-- Table structure for dynamic_case
-- ----------------------------
DROP TABLE IF EXISTS `dynamic_case`;
CREATE TABLE `dynamic_case` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '箱动态id',
  `box_id` int NOT NULL COMMENT '箱号',
  `action` varchar(20) NOT NULL COMMENT '箱动态',
  `place` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '发送地',
  `remarks` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '备注',
  `is_delete` int NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of dynamic_case
-- ----------------------------

-- ----------------------------
-- Table structure for email
-- ----------------------------
DROP TABLE IF EXISTS `email`;
CREATE TABLE `email` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '邮件id',
  `user_id` int NOT NULL,
  `question_type` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '问题类型',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '内容',
  `is_delete` int NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of email
-- ----------------------------

-- ----------------------------
-- Table structure for eval_info
-- ----------------------------
DROP TABLE IF EXISTS `eval_info`;
CREATE TABLE `eval_info` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '评价信息id',
  `order_id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '提单号',
  `user_id` int NOT NULL COMMENT '用户id',
  `content` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '内容',
  `score` int NOT NULL COMMENT '分数',
  `is_delete` int NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of eval_info
-- ----------------------------

-- ----------------------------
-- Table structure for flight
-- ----------------------------
DROP TABLE IF EXISTS `flight`;
CREATE TABLE `flight` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '航班id',
  `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '班期',
  `type` varchar(20) NOT NULL COMMENT '航线组',
  `start_place` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '出发地点',
  `end_place` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '目的地',
  `start_time` datetime NOT NULL COMMENT '出发时间',
  `days` int NOT NULL COMMENT '航程',
  `price` decimal(10,2) NOT NULL COMMENT '运价',
  `is_delete` int NOT NULL COMMENT '逻辑删除字段',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of flight
-- ----------------------------

-- ----------------------------
-- Table structure for inquiryfeedback
-- ----------------------------
DROP TABLE IF EXISTS `inquiryfeedback`;
CREATE TABLE `inquiryfeedback` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '询价回复id',
  `admin_id` int NOT NULL,
  `inquiry_id` int NOT NULL,
  `price` double NOT NULL,
  `flight_id` int NOT NULL,
  `remarks` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `isDelete` int NOT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of inquiryfeedback
-- ----------------------------

-- ----------------------------
-- Table structure for inquiry_information
-- ----------------------------
DROP TABLE IF EXISTS `inquiry_information`;
CREATE TABLE `inquiry_information` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '询价id',
  `user_id` int NOT NULL COMMENT '用户id',
  `start_station` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '起始地',
  `destination` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '目的地',
  `start_time` datetime NOT NULL COMMENT '出发时间',
  `box_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '箱型',
  `goods` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '货物名称',
  `goods_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '货物类型',
  `goods_weight` double NOT NULL COMMENT '货物重量',
  `other_information` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '其他信息',
  `phone_number` varchar(11) NOT NULL COMMENT '手机号',
  `email` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '邮箱',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `is_delete` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of inquiry_information
-- ----------------------------

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '公告id',
  `admin_id` int NOT NULL COMMENT '管理员id',
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '内容',
  `is_delete` int NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of notice
-- ----------------------------

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `order_id` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '提单号',
  `flight_id` int NOT NULL COMMENT '航班id',
  `times` int NOT NULL COMMENT '轮次',
  `pay_type` varchar(10) NOT NULL DEFAULT '1' COMMENT '付款方式，1,2,3.....各代表一种交付方式',
  `weights` double(20,0) NOT NULL COMMENT '总重量',
  `numbers` int NOT NULL COMMENT '总件数',
  `volumes` double(20,0) NOT NULL COMMENT '总体积',
  `deliver_type` varchar(10) NOT NULL DEFAULT '1' COMMENT '交付方式，1,2,3.....各代表一种交付方式',
  `product_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '货物名',
  `is_cable_release` int NOT NULL DEFAULT '1' COMMENT '是否电放，1为是，0为否',
  `remarks` varchar(255) NOT NULL COMMENT '备注信息',
  `operator` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '营运人',
  `is_pressure_chamber` int NOT NULL DEFAULT '1' COMMENT '是否压箱，1为是，0为否',
  `money` double NOT NULL COMMENT '费用',
  `money_type` varchar(10) NOT NULL DEFAULT '1' COMMENT '币种',
  `user_id` int NOT NULL COMMENT '用户id',
  `order_status` int NOT NULL DEFAULT '0' COMMENT '订单状态，0为未支付，1为已支付',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '逻辑删除，0为未删除，1为已删除',
  `create_time` datetime NOT NULL COMMENT '记录创建时间',
  `update_time` datetime NOT NULL COMMENT '记录更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of order
-- ----------------------------

-- ----------------------------
-- Table structure for order_change
-- ----------------------------
DROP TABLE IF EXISTS `order_change`;
CREATE TABLE `order_change` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '换单信息id',
  `order_id` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '提单号',
  `customs_declaration_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '申报的海关',
  `port_destination` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '目的港口',
  `change_date` datetime NOT NULL COMMENT '换单日期',
  `is_change` int NOT NULL DEFAULT '0' COMMENT '是否已换单，0是未换单，1是已换单',
  `money` double NOT NULL COMMENT '费用',
  `money_type` varchar(10) NOT NULL COMMENT '币种',
  `invoice_number` varchar(20) NOT NULL COMMENT '发票号',
  `is_delete` int NOT NULL DEFAULT '0' COMMENT '逻辑删除，0为未删除，1为已删除',
  `create_time` datetime NOT NULL COMMENT '记录创建时间',
  `update_time` datetime NOT NULL COMMENT '记录更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order_change
-- ----------------------------

-- ----------------------------
-- Table structure for ship
-- ----------------------------
DROP TABLE IF EXISTS `ship`;
CREATE TABLE `ship` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '船id',
  `flight_id` int NOT NULL COMMENT '航班id',
  `name` varchar(20) NOT NULL COMMENT '船名',
  `place` varchar(10) NOT NULL COMMENT '所在地',
  `is_delete` int NOT NULL,
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NOT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ship
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'userid',
  `userName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名称',
  `passWord` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '密码',
  `realName` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名字',
  `phoneNumber` varchar(11) NOT NULL COMMENT '电话号码',
  `telePhone` int DEFAULT NULL COMMENT '固定电话',
  `weChat` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '微信',
  `QQ` int DEFAULT NULL,
  `email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '地址',
  `address` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `zipcode` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '邮政编码',
  `is_delete` int NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------

-- ----------------------------
-- Table structure for user_address
-- ----------------------------
DROP TABLE IF EXISTS `user_address`;
CREATE TABLE `user_address` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '收发货信息id',
  `user_id` int NOT NULL COMMENT '用户id',
  `condignor` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发货人',
  `condignor_address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '发货人地址',
  `condignee` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人',
  `condignee_address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '收货人地址',
  `notifier` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '通知人',
  `notifier_address` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '通知人地址',
  `is_delete` int NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user_address
-- ----------------------------

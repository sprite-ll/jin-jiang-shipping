package com.qf.config;

import com.qf.Filter.TokenFilter;
import com.qf.util.RedisUtils;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.HashSet;
import java.util.Set;
@Data
@Configuration
public class MvcConfig  implements WebMvcConfigurer {
    @Autowired
    private RedisUtils redisUtils;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //配置不需要拦截的请求
        Set<String> set=new HashSet<>();
        set.add("/jjshipping/user/register");
        set.add("/file/getVCode");
        set.add("/jjshipping/user/login");
        registry.addInterceptor(new TokenFilter(getRedisUtils(),set));
    }
}

package com.qf.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.dto.FlightDto;
import com.qf.entity.Flight;
import com.qf.dto.PageEntity;
import com.qf.entity.vo.FlightVO;
import com.qf.response.BaseResponse;
import com.qf.service.IFlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@RestController
@RequestMapping("/flight")
public class FlightController {
    @Autowired
    IFlightService iFlightService;
    /**
     * 获取航班列表
     */
    @GetMapping("/getList")
    public PageEntity<Flight> getList(@RequestParam(defaultValue = "10")long size, @RequestParam(defaultValue = "1") long current){
        return iFlightService.getList(size,current);
    }
    /**
     * 查询航班
     */
    @GetMapping("/getFlight")
    public Flight getFlight(Integer id){
        return iFlightService.getById(id);
    }
    /**
     * 查询航班表里的数量
     */
    @PostMapping("/getCount")
    public int getCount(Flight flight){
        LocalDateTime nowDay=flight.getStartTime();
        flight.setStartTime(null);
        LambdaQueryWrapper<Flight> wrapper=new LambdaQueryWrapper<>(flight);
        if(nowDay!=null)wrapper.ge(Flight::getStartTime,nowDay).lt(Flight::getStartTime,nowDay.plusDays(1));
        return iFlightService.count(wrapper);
    }
    /**
     * 插入航班
     */
    @PostMapping("/insertFlight")
    public void insert(Flight flight){
        flight.insert();
    }

    /**
     * 修改航班
     * @param flight
     */
    @PostMapping("/updateFlight")
    public void updateFlight(Flight flight){
        flight.setCreateTime(LocalDateTime.now());
        flight.setUpdateTime(LocalDateTime.now());
        flight.setIsDelete(0);
        flight.insertOrUpdate();
    }
    /**
     * 删除航班
     */
    @GetMapping("/deleteFlight")
    public void delete(Integer id){
        iFlightService.getBaseMapper().deleteById(id);
    }
    /**
     * 根据条件获取航班列表
     */
    @PostMapping("/getListByType")
    public PageEntity<Flight> getListByType(Flight flight,@RequestParam(defaultValue = "10") long size,@RequestParam(defaultValue = "1")long current){
        System.out.println(flight);
        return iFlightService.getListByType(flight,current,size);
    }
    /**
     * 根据船名查询航班列表
     */
    @PostMapping("/getListByShip")
    public PageEntity<Flight> getListByShip(FlightVO flightVO,@RequestParam(defaultValue = "10") long size,@RequestParam(defaultValue = "1")long current){
        return iFlightService.getListByShip(flightVO,size,current);
    }
    /**
     * 获取列表数量
     */
    @PostMapping("getCountFlight")
    public Integer getCountFlight(FlightVO flightVO){
        return iFlightService.getCountFlight(flightVO);
    }
    /**
     * 船舶动态查询
     */
    @PostMapping("/getShipList")
    public PageEntity<FlightDto> getShipList(FlightVO flightVO,@RequestParam(defaultValue = "10") long size,@RequestParam(defaultValue = "1")long current){
        return iFlightService.getShipList(flightVO,size,current);
    }
    /**
     *
     */
}

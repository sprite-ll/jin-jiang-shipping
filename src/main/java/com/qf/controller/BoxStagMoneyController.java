package com.qf.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.BoxStagDto;
import com.qf.dto.PageEntity;
import com.qf.entity.Box;
import com.qf.entity.BoxStagMoney;
import com.qf.response.BaseResponse;
import com.qf.service.IBoxStagMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@RestController
@RequestMapping("/box-stag-money")
public class BoxStagMoneyController {
    @Autowired
    IBoxStagMoneyService iBoxStagMoneyService;

    /**
     * 插入和更新
     * @param boxStagMoney
     */
    @PostMapping("/save")
     public void save(BoxStagMoney boxStagMoney){
         boxStagMoney.setIsDelete(0);
         if(boxStagMoney.getCreateTime()!=null)boxStagMoney.setCreateTime(LocalDateTime.now());
         boxStagMoney.setUpdateTime(LocalDateTime.now());
         boxStagMoney.insertOrUpdate();
     }
    @GetMapping("/delete")
    public void delete(Integer id){
        iBoxStagMoneyService.removeById(id);
    }
    /**
     * 分页查询
     * @param size
     * @param current
     * @return
     */
    @GetMapping("/getPage")
    public PageEntity<BoxStagMoney> getPage(@RequestParam(defaultValue = "10") long size, @RequestParam(defaultValue = "1") long current){
        IPage<BoxStagMoney> iPage=iBoxStagMoneyService.page(new Page<BoxStagMoney>(current,size));
        return new PageEntity<BoxStagMoney>().setCurrent(current).setSize(size).setTotal(iPage.getTotal()).setList(iPage.getRecords());
    }
    @GetMapping("/getStag")
    public List<BoxStagDto> getStag(String orderId){
        return iBoxStagMoneyService.getStag(orderId);
    }
}

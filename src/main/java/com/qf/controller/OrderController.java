package com.qf.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.qf.common.lang.Result;
import com.qf.dto.OrderDto;
import com.qf.dto.OrderSearchDto;
import com.qf.dto.PageEntity;
import com.qf.entity.Flight;
import com.qf.entity.Order;
import com.qf.entity.OrderChange;
import com.qf.entity.User;
import com.qf.entity.vo.OrderVO;
import com.qf.response.BaseResponse;
import com.qf.service.IOrderService;
import com.qf.util.MethodUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@RestController
@RequestMapping("/order")
@Api(tags = "订单管理")
public class OrderController {
    @Resource
    private IOrderService orderService;
    /**
     * 查询所有换单信息
     */
    @ApiOperation(value = "查询所有订单信息接口")
    @GetMapping("/selectOrder")
    public List<Order> selectOrder(HttpServletRequest httpServletRequest){
        return orderService.list();
    }
    /**
     *  分页，配合查询所有的展示
     * @param
     * @return
     */
    @ApiOperation(value = "分页查询")
    @GetMapping("/getPage/{current}/{size}")
    public IPage<OrderDto> getPage(@PathVariable("current") Integer current, @PathVariable("size") Integer size){
        return orderService.getList(size, current);
    }

    //模糊查询，提供前端的模糊查询接口
    /**
     * 测试数据：
     * OrderId:123456
     * shipName:fd
     * times:1
     * startTime:2021-06-16 00:00:00
     * startPlace:fsd
     * endPlace:fds
     * orderStatus:0
     * @return
     */
    @ApiOperation(value = "搜索-模糊查询订单信息")
    @PostMapping("/getListByParams")
    public IPage<OrderSearchDto> getListByParams(Integer current, Integer size, OrderVO orderVO){
//        System.out.println(orderVO.getOrderId());
        return orderService.selectAll(current,size,orderVO);
    }

    //添加订单--快速下单
    @ApiOperation(value = "用户下单")
    @PostMapping("/addOrder")
    public void addOrder(Order order){
        System.out.println(order);
        order.setCreateTime(LocalDateTime.now());
        order.setUpdateTime(LocalDateTime.now());
        orderService.save(order);
    }



    //改
    //要在测试一下
    @ApiOperation(value = "修改订单信息")
    @PostMapping("/updateOrder")
    public void updateOrder(Order order){
        order.setCreateTime(LocalDateTime.now());
        order.setUpdateTime(LocalDateTime.now());
        orderService.saveOrUpdate(order);
    }



    //删
    @ApiOperation(value = "删除订单信息")
    @DeleteMapping("/deleteOrder")
    public void deleteOrder(String orderId){
        orderService.delById(orderId);
    }
    //插入
    @PostMapping("/insertOrder")
    public void insertOrder(Order order){
        order.setOrderId(MethodUtils.randStr());
        order.setCreateTime(LocalDateTime.now());
        order.setUpdateTime(LocalDateTime.now());
        order.setIsDelete(0);
        order.insert();
    }




    /**
     * 查询所有orderId带 orderId
     * @param orderId
     * @return
     */
    @GetMapping("/getOrderByOrderId")
    public Result getOrderByOrderId(String orderId){

        List<Order> orders = orderService.list(new QueryWrapper<Order>().like("order_id", orderId));
        if(orders == null){
            return new Result().fail("查询失败");

        }
        return new Result().succ(orders);
    }

}

package com.qf.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.dto.OrderChangeDto;
import com.qf.dto.PageEntity;
import com.qf.entity.ChargesDemurrage;
import com.qf.entity.OrderChange;
import com.qf.response.BaseResponse;
import com.qf.service.IOrderChangeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@RestController
@Api(tags = "订单信息变更控制器")
@RequestMapping("/order-change")
public class OrderChangeController {
    @Autowired
    private IOrderChangeService orderChangeService;
    /**
     * 查询所有换单信息
     */
    @ApiOperation(value = "查询所有换单信息接口")
    @GetMapping("/selectOrderChange")
    public List<OrderChange> selectOrderChange(HttpServletRequest httpServletRequest){
        return orderChangeService.list();
    }
    /**
     * 换单号查询换单信息
     */
    /**
     * 提单号：order_change->1->
     * 船名: ship->3->
     * 航次：2
     * 付款方式：orders->2->
     * 是否电放：2
     * 换单日期：1
     * 是否已换单：1
     * 装货港：flight->4->
     * 总重量：2
     * 总件数：2
     * 总体积：2
     * 交付方式：2
     * 货名：2
     * 备注：2
     * 申报海关：1
     * 营运人：2
     * 是否押箱：2
     * 目的港：1
     */
    @ApiOperation(value = "查询换单信息")
    @GetMapping("getByOrderId")
    public OrderChangeDto getByOrderId(String orderId){
        return orderChangeService.getByOrderId(orderId);
    }
    /**
     * 增
     */
    @PostMapping("/addOrderChange")
    @ApiOperation(value = "添加订单变更信息接口")
    public void addOrderChange(OrderChange orderChange){
        System.out.println(orderChange);
        orderChange.setCreateTime(LocalDateTime.now());
        orderChange.setUpdateTime(LocalDateTime.now());
        orderChange.setChangeDate(LocalDateTime.now());
        orderChangeService.save(orderChange);
    }
    /**
     * 删
     */
    @DeleteMapping("/deleteOrderChange/{orderId}")
    @ApiOperation("逻辑删除订单变更信息接口")
    public void deleteById(@PathVariable(value = "orderId") String orderId){
        orderChangeService.deleteById(orderId);//delete from product where id = #{id}
    }
    /**
     * 改
     */
    @PatchMapping("/updateOrderChange")
    @ApiOperation("修改订单变更信息接口")
    public void updateById(@RequestBody OrderChange orderChange){
        orderChange.setChangeDate(LocalDateTime.now());
        orderChange.setCreateTime(LocalDateTime.now());
        orderChange.setUpdateTime(LocalDateTime.now());
        orderChangeService.updateById(orderChange);
    }
}

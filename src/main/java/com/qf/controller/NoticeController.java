package com.qf.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.common.lang.Result;
import com.qf.entity.Notice;
import com.qf.response.BaseResponse;
import com.qf.dto.PageEntity;
import com.qf.service.INoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 * 通知公告
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@RestController
@RequestMapping("/notice")
public class NoticeController {
    @Autowired
    INoticeService iNoticeService;


    /**
     * 获取公告列表
     *
     * @param size    一页的个数
     * @param current 当前页
     * @return 公告列表
     */
    @GetMapping("/getList")
    public PageEntity<Notice> getList(@RequestParam(defaultValue = "10") long size,
                                      @RequestParam(defaultValue = "1") long current) {
        Result result = new Result();
        return iNoticeService.getList(size, current);

    }


    /**
     * 获取所有
     * @return
     */
    @GetMapping("/getAllNotice")
    public List<Notice> getAllNotice() {
        return iNoticeService.list();

    }

    /**
     * 根据id删除公告
     * @param id
     */
    @GetMapping("/deleteNoticeById")
    public void deleteNoticeById(@RequestParam Integer id){
        iNoticeService.removeById(id);
    }

    /**
     * 添加或者修改公告
     */
    @PostMapping("/updateNotice")
    public Notice updateNotice(Notice notice){
        System.out.println("notice = " + notice);

        boolean b = iNoticeService.saveOrUpdate(notice);
        System.out.println("notice.getId() = " + notice.getId());

        //返回更新后的公告
        if (b){
            return notice;

        }else {
            return null;
        }

//        boolean b = iNoticeService.saveOrUpdate(notice,new QueryWrapper<Notice>().eq("id",notice.getId()));
//
//        //返回更新后的公告
//        if (b) {
//            return iNoticeService.getById(notice.getId());
//
//
//        } else {
//            return null;
//
//        }

    }
    /**
     * 插入公告
     */
    @PostMapping("/insertNotice")
    public Notice insertNotice(Notice notice){
        iNoticeService.save(notice);
        //返回插入后的公告
        Notice notice1 =  iNoticeService.getById(notice.getId());
        return notice;
        
    }

    /**
     * 根据id获取公告
     * @param id
     * @return
     */
    @GetMapping("/getNoticeById")
    public Notice getNoticeById(Integer id) {
        System.out.println("id = " + id);
        Notice n = iNoticeService.getById(id);
        System.out.println("n = " + n);
        return n;
    }
}

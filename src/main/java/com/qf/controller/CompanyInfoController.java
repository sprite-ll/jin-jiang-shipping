package com.qf.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.common.lang.Result;
import com.qf.entity.CompanyInfo;
import com.qf.service.ICompanyInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@RestController
@RequestMapping("/company-info")
public class CompanyInfoController {

    @Autowired
    ICompanyInfoService iCompanyInfoService;

    /**
     * 添加或者更新公司信息
     * @param companyInfo
     * @return
     */
    @PostMapping("/saveOrUpInfo")
    public Result saveOrUpInfo(CompanyInfo companyInfo) {
        //默认值
        companyInfo.setIsDelete(0);
        if(companyInfo.getCreateTime()==null)companyInfo.setCreateTime(LocalDateTime.now());
        companyInfo.setUpdateTime(LocalDateTime.now());

        //判断是否添加成功
        boolean b = iCompanyInfoService.saveOrUpdate(companyInfo);
        if (b) {
            System.out.println("companyInfo = " + companyInfo);
            return new Result().succ(200,"成功！",null);

        }else {
            return new Result().fail("添加失败");

        }
    }

    /**
     * 根据用户id获取公司信息
     */
    @GetMapping("/getCompany")
    public Result getCompany(Integer userId){
        return Result.succ(iCompanyInfoService.getOne(new QueryWrapper<CompanyInfo>().eq("user_id",userId)));
    }
    /**
     * 根据id删除公司信息
     * @param id
     * @return
     */
    @GetMapping("/delCompangById")
    public Result delCompangById(Integer id) {
        //判断是否删除成功
        boolean b = iCompanyInfoService.removeById(id);
        if (b) {
            return new Result().succ(null);
        }else {
            return new Result().fail("删除失败");
        }
    }
}

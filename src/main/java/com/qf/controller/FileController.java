package com.qf.controller;
import com.qf.util.RedisUtils;
import com.qf.util.VCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
@RequestMapping("/file")
public class FileController {
    @Autowired
    RedisUtils redisUtils;
    @GetMapping("/getVCode")
    public void  getVCode(HttpServletResponse httpResponse, HttpServletRequest request) throws Exception {
        String type=request.getParameter("type");
        VCodeUtils vCodeUtils=new VCodeUtils();
        VCodeUtils.saveImage(vCodeUtils.getImage(),httpResponse.getOutputStream());
        if(type.equals("register"))redisUtils.set("registerVCode",vCodeUtils.getText());
        else redisUtils.set("loginVCode",vCodeUtils.getText());
    }
}

package com.qf.controller;


import com.qf.entity.Email;
import com.qf.response.BaseResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@RestController
@RequestMapping("/email")
public class EmailController {
    /**
     * 插入邮件
     */
    @PostMapping("/sendEmail")
    public void sendEmail(Email email){
        email.insert();
    }
}

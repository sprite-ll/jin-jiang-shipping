package com.qf.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qf.entity.ChargesDemurrage;
import com.qf.entity.OrderChange;
import com.qf.service.IChargesDemurrageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.ChargesDemDto;
import com.qf.dto.PageEntity;
import com.qf.entity.Box;
import com.qf.entity.ChargesDemurrage;
import com.qf.response.BaseResponse;
import com.qf.service.IChargesDemurrageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@Api(tags = "滞期费控制器")
@RestController
@RequestMapping("/charges-demurrage")
public class ChargesDemurrageController {
    @Autowired
    IChargesDemurrageService iChargesDemurrageService;
    /**
     * 插入和更新滞期费
     */
    @PostMapping("/save")
    public void save(ChargesDemurrage ch){
        ch.setIsDelete(0);
        if(ch.getCreateTime()==null)ch.setCreateTime(LocalDateTime.now());
        ch.setUpdateTime(LocalDateTime.now());
        ch.insertOrUpdate();
    }
    @GetMapping("/delete")
    public void delete(Integer id){
        iChargesDemurrageService.removeById(id);
    }
    /**
     * 分页查询
     * @param size
     * @param current
     * @return
     */
    @GetMapping("/getPage")
    public PageEntity<ChargesDemurrage> getPage(@RequestParam(defaultValue = "10") long size, @RequestParam(defaultValue = "1") long current){
        IPage<ChargesDemurrage> iPage=iChargesDemurrageService.page(new Page<ChargesDemurrage>(current,size));
        return new PageEntity<ChargesDemurrage>().setCurrent(current).setSize(size).setTotal(iPage.getTotal()).setList(iPage.getRecords());
    }
    @GetMapping("/getCharge")
    public List<ChargesDemDto> getCharge(String orderId){
        return iChargesDemurrageService.getCharge(orderId);
    }
}

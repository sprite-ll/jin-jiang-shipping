package com.qf.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.common.lang.Result;
import com.qf.entity.UserAddress;
import com.qf.response.BaseResponse;
import com.qf.service.IUserAddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *  会员中心的收发货人信息
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@RestController
@RequestMapping("/user-address")
public class UserAddressController {
    @Autowired
    IUserAddressService iUserAddressService;


    /**
     * 查询会员所有收发货人
     *
     * @return List<UserAddress>
     */
    @GetMapping("/getAllUserAddress")
    public List<UserAddress> getAllUserAdress(Integer id) {
        return iUserAddressService.list(new QueryWrapper<UserAddress>().lambda().eq(UserAddress::getUserId,id));
    }

    /**
     * 添加收获人
     *
     * @param userAddress
     * @return
     */
    @PostMapping("/addUserAddress")
    public UserAddress addUserAddress(UserAddress userAddress) {
        iUserAddressService.save(userAddress);
        return userAddress;
    }

    /**
     * 根据id删除收发货人
     *
     * @param id
     * @return
     */
    @GetMapping("/delUserAddressById")
    public Result delUserAddressById(Integer id) {
        boolean b = iUserAddressService.removeById(id);
        System.out.println("b = " + b);
        if (b) {
            return new Result().succ(200, "删除成功", null);

        } else {
            return new Result().fail("删除失败！");

        }
    }


    /**
     * 更新收发货人
     * @param userAddress
     * @return
     */
    @PostMapping("/updateUserAddress")
    public Result updateUserAddress(UserAddress userAddress) {
        //判断是否更新成功
        boolean b = iUserAddressService.saveOrUpdate(userAddress,new QueryWrapper<UserAddress>().eq("user_id",userAddress.getUserId()));

        if (b) {
            //如果成功查询并返回更新后的useraddress
            return new Result().succ(userAddress);
        }else {
            return new Result().fail("更新失败");
        }


    }
}


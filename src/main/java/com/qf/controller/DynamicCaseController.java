package com.qf.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.PageEntity;
import com.qf.entity.DynamicCase;
import com.qf.service.IDynamicCaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@RestController
@RequestMapping("/dynamic-case")
public class DynamicCaseController {
    @Autowired
    IDynamicCaseService iDynamicCaseService;

    /**
     * 插入或更新
     * @param dynamicCase
     */
    @PostMapping("/save")
    public void save(DynamicCase dynamicCase){
        dynamicCase.setIsDelete(0);
        if(dynamicCase.getCreateTime()!=null)dynamicCase.setCreateTime(LocalDateTime.now());
        dynamicCase.setUpdateTime(LocalDateTime.now());
        dynamicCase.insertOrUpdate();
    }
    /**
     * 删除
     */
    @GetMapping("/delete")
    public void delete(Integer id){
        iDynamicCaseService.removeById(id);
    }

    /**
     * 分页查询
     * @param size
     * @param current
     * @return
     */
    @GetMapping("/getPage")
    public PageEntity<DynamicCase> getPage(@RequestParam(defaultValue = "10") long size, @RequestParam(defaultValue = "1") long current){
        IPage<DynamicCase> iPage=iDynamicCaseService.page(new Page<DynamicCase>(current,size));
        return new PageEntity<DynamicCase>().setCurrent(current).setSize(size).setTotal(iPage.getTotal()).setList(iPage.getRecords());
    }
}

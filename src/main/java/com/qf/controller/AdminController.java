package com.qf.controller;


import com.qf.entity.Admin;
import com.qf.entity.User;
import com.qf.response.BaseResponse;
import com.qf.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@RestController
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    IAdminService iAdminService;
    @PostMapping("/login")
    public Admin login(String name,String password){
        return iAdminService.login(name,password);
    }
    @RequestMapping("/test")
    public User test(String str, HttpServletRequest request){
        User user=(User) request.getAttribute("user");
        return user;
    }
}

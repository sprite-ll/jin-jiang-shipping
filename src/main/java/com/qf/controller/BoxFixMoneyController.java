package com.qf.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.BoxFixDto;
import com.qf.dto.PageEntity;
import com.qf.entity.BoxFixMoney;
import com.qf.entity.BoxStagMoney;
import com.qf.response.BaseResponse;
import com.qf.service.IBoxFixMoneyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@RestController
@RequestMapping("/box-fix-money")
public class BoxFixMoneyController {
    @Autowired
    IBoxFixMoneyService iBoxFixMoneyService;
    /**
     * 插入和更新
     */
    @PostMapping("/save")
    public void save(BoxFixMoney boxFixMoney){
        boxFixMoney.setIsDelete(0);
        if(boxFixMoney.getCreateTime()!=null)boxFixMoney.setCreateTime(LocalDateTime.now());
        boxFixMoney.setUpdateTime(LocalDateTime.now());
        boxFixMoney.insertOrUpdate();
    }
    /**
     * 删除
     */
    @GetMapping("/delete")
    public void delete(Integer id){
        iBoxFixMoneyService.removeById(id);
    }
    /**
     * 分页查询
     * @param size
     * @param current
     * @return
     */
    @GetMapping("/getPage")
    public PageEntity<BoxFixMoney> getPage(@RequestParam(defaultValue = "10") long size, @RequestParam(defaultValue = "1") long current){
        IPage<BoxFixMoney> iPage=iBoxFixMoneyService.page(new Page<BoxFixMoney>(current,size));
        return new PageEntity<BoxFixMoney>().setCurrent(current).setSize(size).setTotal(iPage.getTotal()).setList(iPage.getRecords());
    }
    /**
     * 获取修箱费
     */
    @GetMapping("/getBoxFixs")
    public List<BoxFixDto> getBoxFixs(String orderId){
        return iBoxFixMoneyService.getBoxFixs(orderId);
    }
}

package com.qf.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.BoxActionDto;
import com.qf.dto.PageEntity;
import com.qf.entity.Box;
import com.qf.entity.DynamicCase;
import com.qf.response.BaseResponse;
import com.qf.service.IBoxService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@RestController
@RequestMapping("/box")
public class BoxController {
    @Autowired
    IBoxService iBoxService;
    /**
     * 插入和修改箱数据
     */
    @PostMapping("/saveBox")
    public void insertOrUpdateBox(Box box){
        box.setIsDelete(0);
        if(box.getCreateTime()!=null)box.setCreateTime(LocalDateTime.now());
        box.setUpdateTime(LocalDateTime.now());
        box.insertOrUpdate();
    }
    /**
     * 删除箱数据
     */
    @GetMapping("/deleteBox")
    public void deleteBox(int id){
        iBoxService.removeById(id);
    }
    @GetMapping("/getBoxs")
    public List<BoxActionDto> getBoxs(String orderId, String boxId){
        return iBoxService.getBoxs(orderId,boxId);
    }

    /**
     * 分页查询
     * @param size
     * @param current
     * @return
     */
    @GetMapping("/getPage")
    public PageEntity<Box> getPage(@RequestParam(defaultValue = "10") long size, @RequestParam(defaultValue = "1") long current){
        IPage<Box> iPage=iBoxService.page(new Page<Box>(current,size));
        return new PageEntity<Box>().setCurrent(current).setSize(size).setTotal(iPage.getTotal()).setList(iPage.getRecords());
    }

}

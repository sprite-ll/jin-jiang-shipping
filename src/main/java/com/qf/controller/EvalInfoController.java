package com.qf.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.common.lang.Result;
import com.qf.entity.EvalInfo;
import com.qf.entity.UserAddress;
import com.qf.service.IEmailService;
import com.qf.service.IEvalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 * 会员中心 评论
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@RestController
@RequestMapping("/eval-info")
public class EvalInfoController {
    @Autowired
    IEvalInfoService iEvalInfoService;


    /**
     * 查询所有评论
     * @
     */
    @GetMapping("/getAllEvalInfo")
    public Result getAllEvalInfo() {
        return new Result().succ(200, "查询成功！", iEvalInfoService.list());
    }


    /**
     * 分页查询评论信息
     *
     * @param size
     * @param current
     * @return
     */
    @GetMapping("/getPageEval")
    public Result getPageEvalInfo(@RequestParam(defaultValue = "10") long size,
                                  @RequestParam(defaultValue = "1") long current) {

        return new Result().succ(200, "成功！", iEvalInfoService.getList(size, current));
    }

    /**
     * 添加或者更新评论
     *
     * @param evalInfo 对象
     * @return
     */
    @PostMapping("/saveOrUpEvalInfo")
    public Result saveOrUpEvalInfo(EvalInfo evalInfo) {

        evalInfo.setCreateTime(LocalDateTime.now());
        System.out.println("evalInfo = " + evalInfo);
        //插入数据库
        boolean b = iEvalInfoService.saveOrUpdate(evalInfo);
        //查询添加的结果
        if (b) {
            //添加成功返回添加之后的数据
            return new Result().succ(200, "添加成功", evalInfo);
        } else {
            return new Result().fail("添加失败");
        }
    }

    /**
     * 根据id删除评论
     * @param id
     * @return
     */
    @GetMapping("/delEvalById")
    public Result delEvalById(Integer id) {
        System.out.println("id = " + id);
        //判断是否成功删除评论
        boolean b =  iEvalInfoService.removeById(id);

        if (b) {
            //删除成功
            return new Result().succ(200,"删除成功",null);
        }else {
            return new Result().fail("删除失败");

        }
    }
}

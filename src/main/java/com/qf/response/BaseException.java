package com.qf.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务异常类，继承运行时异常，确保事务正常回滚
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseException extends RuntimeException{
    private ResponseCode code;

    public BaseException(ResponseCode code) {
        super(code.getMsg());
        this.code=code;
    }

    public BaseException(Throwable cause, ResponseCode code) {
        super(code.getMsg(),cause);
        this.code = code;
    }
}

package com.qf.response;

public enum ResponseCode {
    /**
     * 成功返回的状态码
     */
    SUCCESS(200, "success"),
    /**
     * 资源不存在的状态码
     */
    RESOURCES_NOT_EXIST(10201, "资源不存在"),
    /**
     * 所有无法识别的异常默认的返回状态码
     */
    SERVICE_ERROR(50000, "服务器异常"),
    /**
     * 账号不存在
     */
    ID_FALSE(10001,"账号错误"),
    /**
     * 密码错误
     */
    Password_False(10002,"密码错误"),
    /**
     * 未登录
     */
    NOT_LOGIN(10003,"未登录"),
    /**
     * 没有权限
     */
    NOT_HAVE_ROLE(10004,"没有权限"),
    /**
     * 该账号已被注册
     */
    EXIST(10005,"账号已被注册"),
    /**
     * 插入的数据为空
     */
    DATANULL(10006,"插入的数据为空"),
    /**
     * 验证码错误
     */
    VCODE_ERROR(10007,"验证码错误");

    /**
     * 状态码
     */
    private int code;
    /**
     * 返回信息
     */
    private String msg;

    ResponseCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}


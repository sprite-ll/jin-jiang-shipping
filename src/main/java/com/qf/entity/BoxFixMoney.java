package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class BoxFixMoney extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 修箱费号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 箱表id
     */
    private Integer boxId;

    /**
     * 还箱时间
     */
    private LocalDateTime returnTime;

    /**
     * 工时费用
     */
    private Double workMoney;

    /**
     * 材料费用
     */
    private Double materialMonry;

    /**
     * 发票号码
     */
    private String ticketId;

    /**
     * 收款
     */
    private Double receiveMoney;

    /**
     * 收款方式
     */
    private String receiveType;

    /**
     * 收款人
     */
    private String receivePerson;

    /**
     * 收款时间
     */
    private LocalDateTime receiveTime;

    /**
     * 最后动态
     */
    private String lastVictivity;

    /**
     * 是否删除
     */
    private Integer isDelete;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}

package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Box extends Model {

    private static final long serialVersionUID = 1L;
    /**
     * 箱表id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 箱号
     */
    private String boxId;

    /**
     * 提单号
     */
    private String shipId;

    /**
     * 铅封号
     */
    private Integer leadId;

    /**
     * 箱型
     */
    private String boxType;

    /**
     * 箱子尺寸
     */
    private Double boxSize;

    /**
     * 箱重
     */
    private Double boxWeight;

    /**
     * 是否删除
     */
    private Integer isDelete;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}

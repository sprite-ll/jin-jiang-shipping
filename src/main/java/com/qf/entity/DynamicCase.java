package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class DynamicCase extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 箱动态id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 箱号
     */
    private String boxId;

    /**
     * 箱动态
     */
     private String action;

    /**
     * 发送地
     */
    private String place;

    /**
     * 备注
     */
    private String remarks;

    private Integer isDelete;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}

package com.qf.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("orders")
public class Order extends Model {

    private static final long serialVersionUID = 1L;
    /**
     * 订单表id
     */
    @TableId(value = "id", type = IdType.AUTO)
     private Integer id;
    /**
     * 提单号
     */
//    @TableId(value="orderId" ,type = IdType.ASSIGN_UUID)
    private String orderId;

    /**
     * 航班id
     */
    private Integer flightId;
    /**
     * 箱表id
     */
    private Integer boxId;

    /**
     * 轮次
     */
    private Integer times;

    /**
     * 付款方式，1,2,3.....各代表一种交付方式
     */
    private String payType;

    /**
     * 总重量
     */
    private Double weights;

    /**
     * 总件数
     */
    private Integer numbers;

    /**
     * 总体积
     */
    private Double volumes;

    /**
     * 交付方式
     */
    private String deliverType;

    /**
     * 货物名
     */
    private String productName;

    /**
     * 是否电放，1为是，0为否
     */
    private Integer isCableRelease;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 营运人
     */
    private String operator;

    /**
     * 是否压箱，1为是，0为否
     */
    private Integer isPressureChamber;

    /**
     * 费用
     */
    private Double money;

    /**
     * 币种
     */
    private String moneyType;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 订单状态，0为未支付，1为已支付
     */
    private Integer orderStatus;

    /**
     * 逻辑删除，0为未删除，1为已删除
     */
    @TableLogic
    private Integer isDelete;

    /**
     * 记录创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 记录更新时间productName
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


}

package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Inquiryfeedback extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 询价回复id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer adminId;

    private Integer inquiryId;

    private Double price;

    private Integer flightId;

    private String remarks;

    @TableField("isDelete")
    private Integer isDelete;

    @TableField("createTime")
    private LocalDateTime createTime;

    @TableField("updateTime")
    private LocalDateTime updateTime;


}

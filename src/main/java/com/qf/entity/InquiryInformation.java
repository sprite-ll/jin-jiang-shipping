package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class InquiryInformation extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 询价id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 起始地
     */
    private String startStation;

    /**
     * 目的地
     */
    private String destination;

    /**
     * 出发时间
     */
    private LocalDateTime startTime;

    /**
     * 箱型
     */
    private String boxType;

    /**
     * 货物名称
     */
    private String goods;

    /**
     * 货物类型
     */
    private String goodsType;

    /**
     * 货物重量
     */
    private Double goodsWeight;

    /**
     * 其他信息
     */
    private String otherInformation;

    /**
     * 手机号
     */
    private String phoneNumber;

    /**
     * 邮箱
     */
    private String email;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;

    private Integer isDelete;


}

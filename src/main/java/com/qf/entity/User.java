package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class User extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * userid
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户名称
     */
    @TableField("userName")
    private String userName;

    /**
     * 密码
     */
    @TableField("passWord")
    private String passWord;

    /**
     * 用户名字
     */
    @TableField("realName")
    private String realName;

    /**
     * 电话号码
     */
    @TableField("phoneNumber")
    private String phoneNumber;

    /**
     * 固定电话
     */
    @TableField("telePhone")
    private Integer telePhone;

    /**
     * 微信
     */
    @TableField("weChat")
    private String weChat;

    @TableField("QQ")
    private String qq;

    /**
     * 地址
     */
    private String email;

    private String address;

    /**
     * 邮政编码
     */
    private String zipcode;

    private Integer isDelete;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}

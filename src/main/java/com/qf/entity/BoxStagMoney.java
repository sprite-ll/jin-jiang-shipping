package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class BoxStagMoney extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 滞箱费号
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 箱表id
     */
    private Integer boxId;

    /**
     * 使用天数
     */
    private Integer daysUse;

    /**
     * 免费天数
     */
    private Integer daysFree;

    /**
     * 减免类型
     */
    private String reduceType;

    /**
     * 减免数额
     */
    private Double reduceNum;

    /**
     * 币种
     */
    private String currency;

    /**
     * 应收费用
     */
    private Double moneyShould;

    /**
     * 减免费用
     */
    private Double moneyReduce;

    /**
     * 结算金额
     */
    private Double moneyPayed;

    /**
     * 发票号码
     */
    private String ticketId;

    /**
     * 收款
     */
    private Double moneyRecieve;

    /**
     * 收款方式
     */
    private String recieveMethod;

    /**
     * 收款人
     */
    private String recievePerson;

    /**
     * 收款时间
     */
    private LocalDateTime recieveTime;

    /**
     * 是否删除
     */
    private Integer isDelete;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;


}

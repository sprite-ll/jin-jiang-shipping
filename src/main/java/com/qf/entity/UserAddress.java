package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class UserAddress extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 收发货信息id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 发货人
     */
    private String condignor;

    /**
     * 发货人地址
     */
    private String condignorAddress;

    /**
     * 收货人
     */
    private String condignee;

    /**
     * 收货人地址
     */
    private String condigneeAddress;

    /**
     * 通知人
     */
    private String notifier;

    /**
     * 通知人地址
     */
    private String notifierAddress;

    private Integer isDelete;

    private LocalDateTime createTime;

    private LocalDateTime updateTime;


}

package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OrderChange extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 换单信息id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 提单号
     */
    private String orderId;

    /**
     * 申报的海关
     */
    private String customsDeclarationName;

    /**
     * 目的港口
     */
    private String portDestination;

    /**
     * 换单日期
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime changeDate;

    /**
     * 是否已换单，0是未换单，1是已换单
     */
    private Integer isChange;

    /**
     * 费用
     */
    private Double money;

    /**
     * 币种
     */
    private String moneyType;

    /**
     * 发票号
     */
    private String invoiceNumber;

    /**
     * 逻辑删除，0为未删除，1为已删除
     */
    @TableLogic
    private Integer isDelete;

    /**
     * 记录创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 记录更新时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


}

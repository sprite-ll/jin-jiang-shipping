package com.qf.entity.vo;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
public class OrderVO {
    String orderId;
    String shipName;
    Integer times;
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    LocalDateTime startTime;
    String startPlace;
    String endPlace;
    Integer orderStatus;


}

package com.qf.entity.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class FlightVO {
    private String startPlace;
    private String endPlace;
    private String flightName;
    private String shipName;
    private String nowPlace;
}

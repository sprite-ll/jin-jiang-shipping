package com.qf.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Flight extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 航班id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 班期
     */
    private String name;

    /**
     * 航线组
     */
    private String type;

    /**
     * 出发地点
     */
    private String startPlace;

    /**
     * 目的地
     */
    private String endPlace;

    /**
     * 船id
     */
    private Integer shipId;

    /**
     * 所在地
     */
    private String nowPlace;
    /**
     * 出发时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    /**
     * 航程
     */
    private Integer days;

    /**
     * 运价
     */
    private Double price;

    /**
     * 逻辑删除字段
     */
    private Integer isDelete;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 修改时间
     */
    private LocalDateTime updateTime;


}

package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ChargesDemurrage extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 滞期费id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 提单号
     */
    private String orderId;

    /**
     * 进出口，默认0,0为进口，1为出口
     */
    private Integer isImport;

    /**
     * 营运人
     */
    private String operator;

    /**
     * 收费类目
     */
    private String chargeType;

    /**
     * 币种
     */
    private String moneyType;

    /**
     * 结算费用
     */
    private Double money;

    /**
     * 费用减免
     */
    private Double moneyReduction;

    /**
     * 应收费用
     */
    private Double moneyReceivable;

    /**
     * 用箱人
     */
    private String userBox;

    /**
     * 逻辑删除，0为未删除，1为已删除
     */
    @TableLogic
    private Integer isDelete;

    /**
     * 记录创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    /**
     * 记录更新时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


}

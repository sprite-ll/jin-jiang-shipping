package com.qf.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class CompanyInfo extends Model {

    private static final long serialVersionUID = 1L;

    /**
     * 公司信息id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 公司名
     */
    private String name;

    /**
     * 地址
     */
    private String address;

    /**
     * 传真
     */
    private String fax;

    /**
     * 电话
     */
    private String phoneNum;

    /**
     * 营业执照编号
     */
    private String businessId;

    /**
     * 是否增值税一般纳税人
     */
    private Integer ratepayer;

    /**
     * 税务登记证编号
     */
    private String taxId;

    /**
     * 主用户身份证号
     */
    private String cardId;

    /**
     * 名片
     */
    private String callingCard;

    /**
     * 贸易类型
     */
    private String tradeType;

    private Integer isDelete;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updateTime;


}

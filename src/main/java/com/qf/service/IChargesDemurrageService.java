package com.qf.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.qf.dto.ChargesDemDto;
import com.qf.entity.ChargesDemurrage;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface IChargesDemurrageService extends IService<ChargesDemurrage> {

    List<ChargesDemDto> getCharge(String orderId);
}

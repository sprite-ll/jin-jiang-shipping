package com.qf.service;

import com.qf.dto.PageEntity;
import com.qf.entity.EvalInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.entity.Notice;
/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface IEvalInfoService extends IService<EvalInfo> {

    /**
     * 获取通知列表
     * @param size
     * @param current
     * @return
     */
    PageEntity<EvalInfo> getList(long size, long current);
}

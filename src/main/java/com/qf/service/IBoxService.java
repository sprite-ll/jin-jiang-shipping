package com.qf.service;

import com.qf.entity.Box;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.dto.BoxActionDto;
import com.qf.dto.PageEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface IBoxService extends IService<Box> {
    /**
     * 获取方箱列表
     */
    PageEntity<BoxActionDto> getList(long size, long current);

    List<BoxActionDto> getBoxs(String orderId, String boxId);
}

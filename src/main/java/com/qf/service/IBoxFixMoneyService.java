package com.qf.service;

import com.qf.dto.BoxFixDto;
import com.qf.entity.BoxFixMoney;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.dto.PageEntity;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface IBoxFixMoneyService extends IService<BoxFixMoney> {
    /**
     * 获取修箱费列表
     */
    PageEntity<BoxFixMoney> getList(long size,long current);

    List<BoxFixDto> getBoxFixs(String orderId);
}

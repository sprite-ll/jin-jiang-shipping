package com.qf.service;

import com.qf.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface IAdminService extends IService<Admin> {
    /**
     * 登录
     * @param name
     * @param password
     * @return
     */
   public Admin login(String name,String password);
}

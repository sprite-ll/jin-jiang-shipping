package com.qf.service;

import com.qf.entity.Notice;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.dto.PageEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface INoticeService extends IService<Notice> {
    /**
     * 获取通知列表
     * @param size
     * @param current
     * @return
     */
    PageEntity<Notice> getList(long size, long current);
}

package com.qf.service;

import com.qf.dto.OrderChangeDto;
import com.qf.entity.OrderChange;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface IOrderChangeService extends IService<OrderChange> {
    OrderChangeDto getByOrderId(String orderId);

    void deleteById(String orderId);
}

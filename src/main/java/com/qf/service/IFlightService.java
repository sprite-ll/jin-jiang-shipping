package com.qf.service;

import com.qf.dto.FlightDto;
import com.qf.entity.Flight;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.dto.PageEntity;
import com.qf.entity.vo.FlightVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface IFlightService extends IService<Flight> {
    /**
     * 获取航班列表
     * @param size
     * @param current
     * @return
     */
    PageEntity<Flight> getList(long size, long current);

    /**
     * 根据条件获取航班列表
     * @param flight
     * @return
     */
    PageEntity<Flight> getListByType(Flight flight,long current,long size);

    PageEntity<Flight> getListByShip(FlightVO flightVO, long size, long current);

    PageEntity<FlightDto> getShipList(FlightVO flightVO, long size, long current);

    Integer getCountFlight(FlightVO flightVO);

}

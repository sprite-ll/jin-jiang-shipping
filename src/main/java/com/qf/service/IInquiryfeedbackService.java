package com.qf.service;

import com.qf.entity.Inquiryfeedback;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface IInquiryfeedbackService extends IService<Inquiryfeedback> {

}

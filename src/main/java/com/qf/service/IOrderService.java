package com.qf.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.OrderDto;
import com.qf.dto.OrderSearchDto;
import com.qf.dto.PageEntity;
import com.qf.entity.Order;
import com.baomidou.mybatisplus.extension.service.IService;
import com.qf.entity.vo.OrderVO;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface IOrderService extends IService<Order> {
    /**
     *  分页获取订单信息
     * @return
     */
    IPage<OrderDto> getList(Integer size, Integer current);
    /**
     *  带参查询订单信息
     * @return
     */
    IPage<OrderSearchDto> selectAll(Integer current, Integer size, OrderVO orderVO);

    void delById(String orderId);
}

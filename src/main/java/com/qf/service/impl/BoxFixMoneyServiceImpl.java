package com.qf.service.impl;

import com.qf.dto.BoxFixDto;
import com.qf.entity.BoxFixMoney;
import com.qf.dao.BoxFixMoneyMapper;
import com.qf.dto.PageEntity;
import com.qf.service.IBoxFixMoneyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Service
public class BoxFixMoneyServiceImpl extends ServiceImpl<BoxFixMoneyMapper, BoxFixMoney> implements IBoxFixMoneyService {
    @Autowired
    private BoxFixMoneyMapper boxFixMoneyMapper;

    @Override
    public PageEntity<BoxFixMoney> getList(long size, long current) {
        return null;
    }

    @Override
    public List<BoxFixDto> getBoxFixs(String orderId) {
        return boxFixMoneyMapper.getBoxFixs(orderId);
    }
}

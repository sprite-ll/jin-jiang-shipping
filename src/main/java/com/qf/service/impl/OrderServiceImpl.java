package com.qf.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.OrderDto;
import com.qf.dto.OrderSearchDto;
import com.qf.dto.PageEntity;
import com.qf.entity.Order;
import com.qf.dao.OrderMapper;
import com.qf.entity.vo.OrderVO;
import com.qf.response.BaseResponse;
import com.qf.service.IOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.sql.SQLOutput;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@BaseResponse
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {
    @Resource
    private OrderMapper orderMapper;

    /**
     * 获取订单信息
     * @return
     */

    @Override
    public IPage<OrderDto> getList(Integer size, Integer current) {
        Page<OrderDto> page = new Page<>(current, size);
        IPage<OrderDto> orderDtos = orderMapper.selectAll(page);
        return orderDtos;
    }

    /**
     * 带参查询订单信息
     * @return
     */
    @Override
    public IPage<OrderSearchDto> selectAll(Integer current, Integer size, OrderVO orderVO) {
//        System.out.println(current+"=="+size);
        Page page = new Page<>(current,size);
        IPage<OrderSearchDto> orderSearch =orderMapper.selectByParams(page,orderVO);
        return orderSearch;
    }


    @Override
    public void delById(String orderId) {
        orderMapper.delById(orderId);
    }



}

package com.qf.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.FlightDto;
import com.qf.entity.Flight;
import com.qf.dao.FlightMapper;
import com.qf.dto.PageEntity;
import com.qf.entity.vo.FlightVO;
import com.qf.service.IFlightService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Service
public class FlightServiceImpl extends ServiceImpl<FlightMapper, Flight> implements IFlightService {
    @Autowired
    FlightMapper flightMapper;
    /**
     * 获取航班列表
     * @param size
     * @param current
     * @return
     */
    @Override
    public PageEntity<Flight> getList(long size, long current) {
        Page<Flight> page=new Page<>(current,size);
        IPage<Flight> iPage=this.page(page);
        System.out.println(size+"=="+current);
        System.out.println(iPage.getRecords());
        return new PageEntity<Flight>().setCurrent(current).setSize(size).setList(iPage.getRecords()).setTotal(iPage.getTotal());
    }

    @Override
    public PageEntity<Flight> getListByType(Flight flight,long current,long size) {
        LocalDateTime nowDay=flight.getStartTime();
        flight.setStartTime(null);
        LambdaQueryWrapper<Flight> wrapper=new LambdaQueryWrapper<>(flight);
        if(nowDay!=null)wrapper.ge(Flight::getStartTime,nowDay).lt(Flight::getStartTime,nowDay.plusDays(1));
        Page<Flight> page=new Page<>(current,size);
        IPage<Flight> iPage=this.page(page,wrapper);
        return new PageEntity<Flight>().setCurrent(current).setSize(size).setList(iPage.getRecords()).setTotal(iPage.getTotal());
    }

    @Override
    public PageEntity<Flight> getListByShip(FlightVO flightVO, long size, long current) {
        Page<Flight> iPage=new Page<Flight>(current,size);
        return new PageEntity<Flight>().setList(flightMapper.selectFlightByType(iPage,flightVO)).setCurrent(iPage.getCurrent()).setSize(size).setTotal(iPage.getTotal());
    }

    @Override
    public PageEntity<FlightDto> getShipList(FlightVO flightVO, long size, long current) {
        Page page=new Page(current,size);
        return new PageEntity<FlightDto>().setList( flightMapper.selectShipList(flightVO,page)).setTotal(page.getTotal()).setSize(size).setCurrent(current);
    }

    @Override
    public Integer getCountFlight(FlightVO flightVO) {
        return this.getBaseMapper().getCountFlight(flightVO);
    }

}

package com.qf.service.impl;

import com.qf.dto.BoxStagDto;
import com.qf.entity.BoxStagMoney;
import com.qf.dao.BoxStagMoneyMapper;
import com.qf.service.IBoxStagMoneyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Service
public class BoxStagMoneyServiceImpl extends ServiceImpl<BoxStagMoneyMapper, BoxStagMoney> implements IBoxStagMoneyService {
    @Autowired
    BoxStagMoneyMapper boxStagMoneyMapper;

    @Override
    public List<BoxStagDto> getStag(String orderId) {
        return boxStagMoneyMapper.getStag(orderId);
    }
}

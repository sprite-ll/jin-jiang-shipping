package com.qf.service.impl;

import com.qf.entity.Inquiryfeedback;
import com.qf.dao.InquiryfeedbackMapper;
import com.qf.service.IInquiryfeedbackService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Service
public class InquiryfeedbackServiceImpl extends ServiceImpl<InquiryfeedbackMapper, Inquiryfeedback> implements IInquiryfeedbackService {

}

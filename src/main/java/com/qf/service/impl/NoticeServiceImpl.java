package com.qf.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.entity.Notice;
import com.qf.dao.NoticeMapper;
import com.qf.dto.PageEntity;
import com.qf.service.INoticeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Service
public class NoticeServiceImpl extends ServiceImpl<NoticeMapper, Notice> implements INoticeService {

    /**
     * 获取公告列表
     * @param size
     * @param current
     * @return
     */
    @Override
    public PageEntity<Notice> getList(long size, long current) {
        Page<Notice> page=new Page<>(current,size);
        IPage<Notice> page1=this.page(page,new QueryWrapper<Notice>());
        return new PageEntity<Notice>().setCurrent(current).setList(page1.getRecords()).
                setSize(size).setTotal(page1.getTotal());
    }
}

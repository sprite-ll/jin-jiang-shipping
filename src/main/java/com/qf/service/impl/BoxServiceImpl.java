package com.qf.service.impl;

import com.qf.entity.Box;
import com.qf.dao.BoxMapper;
import com.qf.dto.BoxActionDto;
import com.qf.dto.PageEntity;
import com.qf.service.IBoxService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Service
public class BoxServiceImpl extends ServiceImpl<BoxMapper, Box> implements IBoxService {
    @Autowired
    private BoxMapper boxMapper;

    @Override
    public PageEntity<BoxActionDto> getList(long size, long current) {
        return null;
    }

    @Override
    public List<BoxActionDto> getBoxs(String orderId, String boxId) {
        return boxMapper.getBoxs(orderId,boxId);
    }
}

package com.qf.service.impl;

import com.qf.entity.DynamicCase;
import com.qf.dao.DynamicCaseMapper;
import com.qf.service.IDynamicCaseService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Service
public class DynamicCaseServiceImpl extends ServiceImpl<DynamicCaseMapper, DynamicCase> implements IDynamicCaseService {

}

package com.qf.service.impl;

import com.qf.dto.OrderChangeDto;
import com.qf.entity.OrderChange;
import com.qf.dao.OrderChangeMapper;
import com.qf.service.IOrderChangeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */

@Service
public class OrderChangeServiceImpl extends ServiceImpl<OrderChangeMapper, OrderChange> implements IOrderChangeService {
    @Resource
    private OrderChangeMapper orderChangeMapper;

    @Override
    public OrderChangeDto getByOrderId(String orderId) {
        return orderChangeMapper.getByOrderId(orderId);
    }

    @Override
    public void deleteById(String orderId) {
        orderChangeMapper.delById(orderId);
    }
}

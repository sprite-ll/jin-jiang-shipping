package com.qf.service.impl;

import com.qf.entity.InquiryInformation;
import com.qf.dao.InquiryInformationMapper;
import com.qf.service.IInquiryInformationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Service
public class InquiryInformationServiceImpl extends ServiceImpl<InquiryInformationMapper, InquiryInformation> implements IInquiryInformationService {

}

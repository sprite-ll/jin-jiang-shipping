package com.qf.service.impl;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.ChargesDemDto;
import com.qf.entity.ChargesDemurrage;
import com.qf.dao.ChargesDemurrageMapper;
import com.qf.service.IChargesDemurrageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Service
public class ChargesDemurrageServiceImpl extends ServiceImpl<ChargesDemurrageMapper, ChargesDemurrage> implements IChargesDemurrageService {
    @Resource
    private ChargesDemurrageMapper mapper;

    @Override
    public List<ChargesDemDto> getCharge(String orderId) {
        return mapper.selectCharge(orderId);
    }
}

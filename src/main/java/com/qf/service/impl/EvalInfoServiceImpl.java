package com.qf.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.PageEntity;
import com.qf.entity.EvalInfo;
import com.qf.dao.EvalInfoMapper;
import com.qf.entity.Notice;
import com.qf.service.IEvalInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Service
public class EvalInfoServiceImpl extends ServiceImpl<EvalInfoMapper, EvalInfo> implements IEvalInfoService {


    @Override
    public PageEntity<EvalInfo> getList(long size, long current) {
        Page<EvalInfo> page=new Page<>(current,size);
        IPage<EvalInfo> page1=this.page(page,new QueryWrapper<EvalInfo>());
        return new PageEntity<EvalInfo>().setCurrent(current).setList(page1.getRecords()).
                setSize(size).setTotal(page1.getTotal());
    }
}

package com.qf.service;

import com.qf.dto.BoxStagDto;
import com.qf.entity.BoxStagMoney;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface IBoxStagMoneyService extends IService<BoxStagMoney> {

    List<BoxStagDto> getStag(String orderId);
}

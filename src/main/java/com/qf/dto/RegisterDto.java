package com.qf.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class RegisterDto implements Serializable {
    /*
    * 用户名
    * */
    private String userName;

    /**
     * 密码
     */
    private String passWord;

    /**
     * 真实名字或者公司名
     */
    private String realName;


    /**
     * 手机号
     */
    private String phoneNumber;


    /**
     * 邮箱
     */
    private String email;

    /**
     * QQ
     */
    private String QQ;

    /**
     * 电话
     */
    private Integer telePhone;


    /**
     * 微信
     */
    private String weChat;

    /**
     * 地址
     */
    private String address;

    /**
     * 邮政编码
     */
    private String zipcode;

    /**
     * is_delete
     */
    private String is_delete;

}

package com.qf.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;

@Data

public class ChangePersonalInfoDto implements Serializable {

    private Integer id;
    private String userName;

    private String passWord;

    private String realName;

    /**
     * 电话号码
     */
    private String phoneNumber;

    /**
     * 地址
     */
    private String email;



    /**
     * 微信
     */
    private String weChat;

    private String qq;
}

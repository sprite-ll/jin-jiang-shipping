package com.qf.dto;
import lombok.Data;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;
@Data
@Accessors(chain = true)
public class FlightDto  {

    private static final long serialVersionUID = 1L;

    /**
     * 航班id
     */
    private Integer id;

    /**
     * 班期
     */
    private String flightName;

    /**
     * 航线组
     */
    private String type;

    /**
     * 出发地点
     */
    private String startPlace;

    /**
     * 目的地
     */
    private String endPlace;

    /**
     * 出发时间
     */
    private LocalDateTime startTime;

    /**
     * 航程
     */
    private Integer days;

    /**
     * 运价
     */
    private Double price;

    private String shipName;

    private  String nowPlace;
}

package com.qf.dto;

import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;

@Data
public class LoginDto implements Serializable {

    private String userName;

    private String passWord;

}

package com.qf.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class BoxActionDto {
    /**
     * 箱动态id
     */
    private Integer id;

    /**
     * 船名
     */
    private String shipName;

    /**
     * 航次
     */
    private Integer times;

    /**
     * 订单表id
     */
    private String orderId;

    /**
     * 装货港
     */
    private String startPlace;

    /**
     * 卸货港
     */
    private String endPlace;

    /**
     * 箱表id
     */
    private String boxId;

    /**
     * 箱型
     */
    private String boxType;

    /**
     * 箱子尺寸
     */
    private Double boxSize;

    /**
     * 箱重
     */
    private Double boxWeight;

    /**
     * 箱动态
     */
    private String action;

    /**
     * 发送地
     */
    private String place;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 箱动态时间
     */
    private LocalDateTime updateTime;

}

package com.qf.dto;
import lombok.Data;
import lombok.experimental.Accessors;
import java.time.LocalDateTime;
@Data
@Accessors(chain = true)
public class OrderChangeDto {
    /**
     * 换单信息id
     */
    private Integer id;

    /**
     * 提单号
     */
    private String orderId;

    /**
     * 申报的海关
     */
    private String customsDeclarationName;

    /**
     *启航时间
     */
    private LocalDateTime startTime;

    /**
     * 装货港
     */
    private  String startPlace;

    /**
     * 目的港口
     */
    private String portDestination;

    /**
     * 换单日期
     */
    private LocalDateTime changeDate;

    /**
     * 是否已换单，0是未换单，1是已换单
     */
    private Integer isChange;

    /**
     * 费用
     */
    private Double money;

    /**
     * 币种
     */
    private String moneyType;

    /**
     * 发票号
     */
    private String invoiceNumber;

    /**
     * 航班id
     */
    private Long flightId;

    /**
     * 轮次
     */
    private Integer times;

    /**
     * 付款方式，1,2,3.....各代表一种交付方式
     */
    private String payType;

    /**
     * 总重量
     */
    private Double weights;

    /**
     * 总件数
     */
    private Integer numbers;

    /**
     * 总体积
     */
    private Double volumes;

    /**
     * 交付方式
     */
    private String deliverType;

    /**
     * 货物名
     */
    private String productName;

    /**
     * 是否电放，1为是，0为否
     */
    private Integer isCableRelease;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 营运人
     */
    private String operator;

    /**
     * 是否压箱，1为是，0为否
     */
    private Integer isPressureChamber;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 订单状态，0为未支付，1为已支付
     */
    private Integer orderStatus;

    /**
     * 箱表id
     */
    private Integer boxId;
    /**
     * 箱号
     */
    private String realBoxId;

    /**
     * 铅封号
     */
    private Integer leadId;

    /**
     * 箱型
     */
    private String boxType;

    /**
     * 箱子尺寸
     */
    private Double boxSize;

    /**
     * 箱重
     */
    private Double boxWeight;
    /**
     * 航程
     */
    private Integer days;
    /**
     * 船名
     */
    private String name;

}

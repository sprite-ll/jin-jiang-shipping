package com.qf.dto;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ChargesDemDto {
    /**
     * 滞期费id
     */
    private Integer id;

    /**
     * 提单号
     */
    private String orderId;

    /**
     * 船名
     */
    private String shipName;

    /**
     * 航次
     */
    private String flightName;

    /**
     * 进出口，默认0,0为进口，1为出口
     */
    private Integer isImport;

    /**
     * 营运人
     */
    private String operator;

    /**
     * 收费类目
     */
    private String chargeType;

    /**
     * 币种
     */
    private String moneyType;

    /**
     * 结算费用
     */
    private Double money;

    /**
     * 费用减免
     */
    private Double moneyReduction;

    /**
     * 应收费用
     */
    private Double moneyReceivable;

    /**
     * 用箱人
     */
    private String userBox;
}

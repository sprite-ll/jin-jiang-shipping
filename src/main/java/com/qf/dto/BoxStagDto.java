package com.qf.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class BoxStagDto {
    /**
     * 滞箱费id
     */
    private Integer id;
    /**
     * 箱表id
     */
    private Integer boxId;
    /**
     * 箱号
     */
     private String realBoxId;
    /**
     * 箱型
     */
    private String boxType;

    /**
     * 箱子尺寸
     */
    private Double boxSize;

    /**
     * 使用天数
     */
    private Integer daysUse;

    /**
     * 免费天数
     */
    private Integer daysFree;

    /**
     * 减免类型
     */
    private String reduceType;

    /**
     * 减免数额
     */
    private Double reduceNum;

    /**
     * 币种
     */
    private String currency;

    /**
     * 应收费用
     */
    private Double moneyShould;

    /**
     * 减免费用
     */
    private Double moneyReduce;

    /**
     * 结算金额
     */
    private Double moneyPayed;

    /**
     * 发票号码
     */
    private String ticketId;

    /**
     * 收款
     */
    private Double moneyRecieve;

    /**
     * 收款方式
     */
    private String recieveMethod;

    /**
     * 收款人
     */
    private String recievePerson;

    /**
     * 收款时间
     */
    private LocalDateTime recieveTime;



}

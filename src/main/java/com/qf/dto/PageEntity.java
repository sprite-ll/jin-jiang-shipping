package com.qf.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;
@Data
@Accessors(chain = true)
@ToString
public class PageEntity<T> {
    //每页的数量
    private long size;
    //页数
    private long current;
    //总数
    private long total;
    //数据集合
    private List<T> list;
}

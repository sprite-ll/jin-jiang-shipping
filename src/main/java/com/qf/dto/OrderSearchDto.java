package com.qf.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class OrderSearchDto {
    //a.order_id,c.name,a.times,b.start_time,b.start_place,b.end_place,a.order_status

    /**
     * 提单号
     */
    private String orderId;
    /**
     * 船名
     */
    private String shipName;
    /**
     * 轮次
     */
    private Integer times;

    /**
     * 出发地点
     */
    private String startPlace;

    /**
     * 目的地
     */
    private String endPlace;
    /**
     * 海运费
     */
    private double money;
    /**
     * 订单状态，0为未支付，1为已支付
     */
    private Integer orderStatus;
    /**
     * 开航时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

}

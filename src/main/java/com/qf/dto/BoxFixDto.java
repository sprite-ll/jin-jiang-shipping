package com.qf.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
@Data
@Accessors(chain = true)
public class BoxFixDto {
    /**
     * 修箱费号
     */
    private Integer id;

    /**
     * 箱表id
     */
    private Integer boxId;

    /**
     * 箱号
     */
    private String realBoxId;

    /**
     * 箱型
     */
    private String boxType;

    /**
     * 箱子尺寸
     */
    private Double boxSize;

    /**
     * 还箱时间
     */
    private LocalDateTime returnTime;

    /**
     * 工时费用
     */
    private Double workMoney;

    /**
     * 材料费用
     */
    private Double materialMonry;

    /**
     * 发票号码
     */
    private String ticketId;

    /**
     * 收款
     */
    private Double receiveMoney;

    /**
     * 收款方式
     */
    private String receiveType;

    /**
     * 收款人
     */
    private String receivePerson;

    /**
     * 收款时间
     */
    private LocalDateTime receiveTime;

    /**
     * 最后动态
     */
    private String lastVictivity;
}

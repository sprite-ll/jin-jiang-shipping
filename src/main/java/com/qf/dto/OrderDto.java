package com.qf.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class OrderDto {
    /**
     * 订单表id
     */
    private Integer id;
    /**
     * 提单号
     */
    private String orderId;

    /**
     * 班期
     */
    private String flightName;
    /**
     * 船名
     */
    private String shipName;

    /**
     * 轮次
     */
    private Integer times;

    /**
     * 航班id
     */
    private Integer flightId;

    /**
     * 装货港
     */
    private String startPlace;

    /**
     * 卸货港
     */
    private String endPlace;

    /**
     * 开航时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;
    /**
     * 付款方式，1,2,3.....各代表一种交付方式
     */
    private String payType;

    /**
     * 总重量
     */
    private Double weights;

    /**
     * 总件数
     */
    private Integer numbers;

    /**
     * 总体积
     */
    private Double volumes;

    /**
     * 交付方式
     */
    private String deliverType;

    /**
     * 货物名
     */
    private String productName;

    /**
     * 是否电放，1为是，0为否
     */
    private Integer isCableRelease;

    /**
     * 备注信息
     */
    private String remarks;

    /**
     * 营运人
     */
    private String operator;

    /**
     * 是否压箱，1为是，0为否
     */
    private Integer isPressureChamber;

    /**
     * 费用
     */
    private Double money;

    /**
     * 币种
     */
    private String moneyType;

    /**
     * 用户id
     */
    private Integer userId;

    /**
     * 订单状态，0为未支付，1为已支付
     */
    private Integer orderStatus;
}

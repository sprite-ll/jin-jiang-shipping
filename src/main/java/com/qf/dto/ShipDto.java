package com.qf.dto;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class ShipDto {
    /**
     * 航班id
     */
    private Integer flight_id;

    /**
     * 船id
     */
    private Integer shipId;

    /**
     * 船名
     */
    private String shipName;

    /**
     * 所在地
     */
    private String place;

    /**
     * 班期
     */
    private String flightName;

    /**
     * 航线组
     */
    private String type;

    /**
     * 出发地点
     */
    private String startPlace;

    /**
     * 目的地
     */
    private String endPlace;

}

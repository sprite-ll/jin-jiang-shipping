package com.qf.dto;

import lombok.Data;
import lombok.NonNull;

import java.io.Serializable;

/**
 * 修改密码
 */
@Data
public class ChangePswDto implements Serializable {
    @NonNull
    private String userName;
    @NonNull
    private String passWord;

    /**
     * 新密码
     */
    private String newPsw;
}

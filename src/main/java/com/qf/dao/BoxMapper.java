package com.qf.dao;

import com.qf.dto.BoxActionDto;
import com.qf.entity.Box;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface BoxMapper extends BaseMapper<Box> {

    List<BoxActionDto> getBoxs(String orderId, String boxId);
}

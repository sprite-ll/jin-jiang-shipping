package com.qf.dao;

import com.qf.entity.Inquiryfeedback;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface InquiryfeedbackMapper extends BaseMapper<Inquiryfeedback> {

}

package com.qf.dao;

import com.qf.dto.ChargesDemDto;
import com.qf.entity.ChargesDemurrage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface ChargesDemurrageMapper extends BaseMapper<ChargesDemurrage> {
    List<ChargesDemDto> selectCharge(String orderId);

    void delById(String orderId);
}

package com.qf.dao;

import com.qf.dto.OrderChangeDto;
import com.qf.entity.OrderChange;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Mapper
public interface OrderChangeMapper extends BaseMapper<OrderChange> {

    OrderChangeDto getByOrderId(String orderId);

    void delById(String orderId);
}

package com.qf.dao;

import com.qf.dto.BoxFixDto;
import com.qf.entity.BoxFixMoney;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface BoxFixMoneyMapper extends BaseMapper<BoxFixMoney> {

    List<BoxFixDto> getBoxFixs(String orderId);
}

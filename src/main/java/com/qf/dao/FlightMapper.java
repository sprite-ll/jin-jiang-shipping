package com.qf.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.FlightDto;
import com.qf.dto.PageEntity;
import com.qf.entity.Flight;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.vo.FlightVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Mapper
public interface FlightMapper extends BaseMapper<Flight> {

    public List<Flight> selectFlightByType(Page<Flight> page, FlightVO flightVO);


    List<FlightDto> selectShipList(FlightVO flightVO, Page page);

    Integer getCountFlight(FlightVO flightVO);
}

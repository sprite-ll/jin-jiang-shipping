package com.qf.dao;

import com.qf.dto.BoxStagDto;
import com.qf.entity.BoxStagMoney;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
public interface BoxStagMoneyMapper extends BaseMapper<BoxStagMoney> {

    List<BoxStagDto> getStag(String orderId);
}

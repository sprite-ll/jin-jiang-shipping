package com.qf.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qf.dto.OrderDto;
import com.qf.dto.OrderSearchDto;
import com.qf.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.entity.vo.OrderVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author astupidcoder
 * @since 2021-06-08
 */
@Mapper
public interface OrderMapper extends BaseMapper<Order> {
    IPage<OrderDto> selectAll(Page<OrderDto> page);

    IPage<OrderSearchDto> selectByParams(Page<OrderSearchDto> page, OrderVO orderVO);

    void delById(String orderId);

}

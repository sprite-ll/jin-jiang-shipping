package com.qf.jinjiangshipping_pxxy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication(scanBasePackages = "com")
@MapperScan("com.qf.dao")
public class JinjiangshippingPxxyApplication {

    public static void main(String[] args) {
        SpringApplication.run(JinjiangshippingPxxyApplication.class, args);
    }

}

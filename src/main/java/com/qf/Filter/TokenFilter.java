package com.qf.Filter;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.qf.entity.User;
import com.qf.response.BaseException;
import com.qf.response.ResponseCode;
import com.qf.util.RedisUtils;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@Log
@AllArgsConstructor
public class TokenFilter implements HandlerInterceptor {
    private RedisUtils redisUtils;
    private Set<String> set;
    //接口处理前
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String uri=request.getRequestURI();
//        if(!set.contains(uri)){
//            String token=request.getHeader("Authorize");
//            if(StringUtils.isEmpty(token))throw new BaseException(ResponseCode.NOT_LOGIN);
//            User user=(User)redisUtils.get(token);
//            if(user==null)throw new BaseException(ResponseCode.NOT_LOGIN);
//            request.setAttribute("user",user);
//        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}

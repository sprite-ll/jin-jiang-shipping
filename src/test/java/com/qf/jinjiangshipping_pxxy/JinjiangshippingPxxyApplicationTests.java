package com.qf.jinjiangshipping_pxxy;

import com.qf.dao.NoticeMapper;
import com.qf.entity.EvalInfo;
import com.qf.entity.Notice;
import com.qf.entity.User;
import com.qf.entity.UserAddress;
import com.qf.service.IEvalInfoService;
import com.qf.service.INoticeService;
import com.qf.service.IUserAddressService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.dao.FlightMapper;
import com.qf.dao.UserMapper;
import com.qf.entity.User;
import com.qf.entity.vo.FlightVO;
import com.qf.service.IFlightService;
import com.qf.util.MethodUtils;
import com.qf.util.RedisUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;

import java.time.LocalDateTime;
import java.util.List;

@SpringBootTest
class JinjiangshippingPxxyApplicationTests {
    @Autowired
    RedisUtils redisUtils;
    @Autowired
    INoticeService iNoticeService;
    @Autowired
    IUserAddressService iUserAddressService;
    @Autowired
    IEvalInfoService iEvalInfoService;



    UserMapper userMapper;
    @Autowired
    IFlightService iFlightService;
    @Autowired
    FlightMapper flightMapper;

    @Test
    void contextLoads() {
        redisUtils.set("name","rmj");
        redisUtils.set("user",new User().setAddress("23").setEmail("ewdsfd"));
        System.out.println(redisUtils.get("name"));
        System.out.println(redisUtils.get("user"));
    }
    @Test
    public void testRandStr(){
        System.out.println(MethodUtils.randStr());
    }

    @Test
    public void addNotice() {
        for (int i = 0; i <9 ; i++) {

            Notice notice = new Notice();
            notice.setAdminId(i);
            notice.setTitle("标题" + i);
            notice.setContent("内容内容");
            iNoticeService.save(notice);
        }
    }


    //添加评价
    @Test
    public void addEvalInfo() {
        for (int i = 0; i <20 ; i++) {

            EvalInfo evalInfo= new EvalInfo();
            evalInfo.setUserId(i);
            evalInfo.setOrderId("Id"+i);
            evalInfo.setContent("setContent");
            evalInfo.setCreateTime( LocalDateTime.now());
            evalInfo.setScore(99);
            iEvalInfoService.save(evalInfo);
        }
    }

    //添加收货人
    @Test
    public void addUserAddress() {
        for (int i = 0; i <20 ; i++) {

            UserAddress userAddress= new UserAddress();
            userAddress.setUserId(i);
            userAddress.setCondignee("setCondignee" + i);
            userAddress.setCondigneeAddress("setCondigneeAddress"+i);
            userAddress.setCondignor("setCondignor");
            userAddress.setCondignorAddress("setCondignorAddress");
            userAddress.setCondignor("setCondignor");
            iUserAddressService.save(userAddress);
        }
    }

    @Test
    public void testMapper(){
        System.out.println(userMapper.selectCount(new QueryWrapper<User>()));
    }
    @Test
    public void testService(){
        //System.out.println(iFlightService.getListByShip(new FlightVO(),10,1));
        System.out.println(iFlightService.getShipList(new FlightVO(),10,1));
    }
    @Test
    public void test1(){
        flightMapper.deleteById(1);
    }
    @Test
    public void testGetCountFlight(){
        flightMapper.getCountFlight(new FlightVO());
    }
}
